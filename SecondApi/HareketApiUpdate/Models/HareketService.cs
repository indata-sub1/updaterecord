﻿using SetCRMHelper;
using SetCRMHelper.MayaModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HareketApiUpdate.Models
{
    public class HareketService : IHareketService
    {
        private readonly ISetCrmHelper _setCrmHelper;

        public HareketService(ISetCrmHelper setCrmHelper)
        {
            _setCrmHelper = setCrmHelper;
        }
        public async Task PauseAsync(HareketInputModel input)
        {
            var apiRequest = new RecordRequestParameters()
            {
                CustomObjectId = "E1938BD94386483284F75EE0304CBA38",
                RecordId = input.RecordId,
                FieldsValues = new Dictionary<string, object>(),
                IsForcingSave = false
            };
            apiRequest.FieldsValues.Add("2A2AA9C0EEAC44AD894B74068C75870B", DateTime.Now);
            apiRequest.FieldsValues.Add("69687C96B8DD40DA8DAC866CE2C4C85A", input.HareketUserId);
            apiRequest.FieldsValues.Add("2777B3843EE840FC999B7F57ED0537F4", "3834D70B62654B07A6A64790B98EC21A");

            await _setCrmHelper.UpdateRecordAsync(apiRequest);
        }

        public async Task PlayAsync(HareketInputModel input)
        {
            var apiRequest = new RecordRequestParameters()
            {
                CustomObjectId = "E1938BD94386483284F75EE0304CBA38",
                RecordId = input.RecordId,
                FieldsValues = new Dictionary<string, object>(),
                IsForcingSave = false
            };
            apiRequest.FieldsValues.Add("2A2AA9C0EEAC44AD894B74068C75870B", DateTime.Now);
            apiRequest.FieldsValues.Add("69687C96B8DD40DA8DAC866CE2C4C85A", input.HareketUserId);
            apiRequest.FieldsValues.Add("2777B3843EE840FC999B7F57ED0537F4", "7DECBF4368C241718D2C952EECE9F825 ");

            await _setCrmHelper.UpdateRecordAsync(apiRequest);
        }

        public async Task StopAsync(HareketInputModel input)
        {
            var apiRequest = new RecordRequestParameters()
            {
                CustomObjectId = "E1938BD94386483284F75EE0304CBA38",
                RecordId = input.RecordId,
                FieldsValues = new Dictionary<string, object>(),
                IsForcingSave = false
            };
            apiRequest.FieldsValues.Add("2A2AA9C0EEAC44AD894B74068C75870B", DateTime.Now);
            apiRequest.FieldsValues.Add("69687C96B8DD40DA8DAC866CE2C4C85A", input.HareketUserId);
            apiRequest.FieldsValues.Add("2777B3843EE840FC999B7F57ED0537F4", "7D2E4A15C7634C6BAFD311D78CBF1357");

            await _setCrmHelper.UpdateRecordAsync(apiRequest);
        }
    }
}
