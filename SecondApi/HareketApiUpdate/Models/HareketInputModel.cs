﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HareketApiUpdate.Models
{
    public class HareketInputModel
    {
        public string HareketUserId { get; set; }
        public string RecordId { get; set; }
    }
}
