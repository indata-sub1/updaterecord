﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HareketApiUpdate.Models
{
    public interface IHareketService
    {
        Task PlayAsync(HareketInputModel input);
        Task PauseAsync(HareketInputModel input);
        Task StopAsync(HareketInputModel input);
    }
}
