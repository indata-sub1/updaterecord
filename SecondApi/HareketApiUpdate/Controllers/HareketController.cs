﻿using HareketApiUpdate.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HareketApiUpdate.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HareketController : ControllerBase
    {
        private readonly IHareketService _hareketService;

        public HareketController(IHareketService hareketService)
        {
            _hareketService = hareketService;
        }
        [HttpPost]
        [Route("/v1/hareket/updatestart")]
        public async Task<IActionResult> UpdateStartAsync(string recordID, [FromBody] HareketInputModel input)
        {
            if (recordID == input.RecordId)
            {
                await _hareketService.PlayAsync(input);
                return Ok(true);
            }
            return Ok(false);

        }
        [HttpPost]
        [Route("/v1/hareket/updatepause")]
        public async Task<IActionResult> UpdatePauseAsync(string recordID, [FromBody] HareketInputModel input)
        {
            if (recordID == input.RecordId)
            {
                await _hareketService.PauseAsync(input);
                return Ok(true);
            }
            return Ok(false);
        }
        [HttpPost]
        [Route("/v1/hareket/updatestop")]
        public async Task<IActionResult> UpdateStopAsync(string recordID, [FromBody] HareketInputModel input)
        {
            if (recordID == input.RecordId)
            {
                await _hareketService.StopAsync(input);
                return Ok(true);
            }
            return Ok(false);
        }
    }
}
